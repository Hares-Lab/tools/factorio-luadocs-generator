"""
This module provides methods for downloading and loading Factorio API from either site or from disk.
Supports resolving *latest* version and caching information on disk.

Usage:
```
from factorio_luadocs_generator.api_loader import load_api

factorio_api = load_api()
```
"""

import glob
import json
import os
from enum import StrEnum, Enum
from pathlib import Path
from typing import *

import requests as requests

from .io import get_local_cache_directory
from .logging_utils import get_logger
from .model import *

logger = get_logger('api-loader')

DEFAULT_API_VERSION: Final[int] = 5
VersionToken = NewType('VersionToken', str)
__LATEST__ = VersionToken('latest')
__STABLE__ = VersionToken('stable')
_VERSION_TOKENS = (__LATEST__, __STABLE__)

class APIKind(Enum):
    RuntimeAPI = 'runtime-api'
    PrototypesAPI = 'prototype-api'

def get_download_url(app_version: str, api_kind: APIKind) -> str:
    return f'https://lua-api.factorio.com/{app_version}/{api_kind.value}.json'

def get_cache_filepath(app_version: str, api_kind: APIKind) -> Path:
    return get_local_cache_directory(app_version) / f'{api_kind.value}.json'

def etag_file(base_file: Path) -> Path:
    return base_file.with_suffix(base_file.suffix + '.etag')

def download_api(dest: Path, *, app_version: str, api_version: int, api_kind: APIKind) -> Path:
    
    logger.info(f"Downloading API definitions for Factorio of version '{app_version}'...")
    resp = requests.get(get_download_url(app_version, api_kind))
    assert resp.ok, f"Can't download factorio API v{api_version} for game version {app_version}."
    
    if (app_version in _VERSION_TOKENS):
        app_version = cast(FactorioDataModel, resp.json())['application_version']
        dest = get_cache_filepath(app_version, api_kind)
    
    dest.parent.mkdir(exist_ok=True, parents=True, mode=0o755)
    dest.write_bytes(resp.content)
    
    if (etag:= resp.headers.get('Etag')):
        etag_file(dest).write_text(etag.strip('"'))
    
    logger.info(f"API definition saved at '{dest}'")
    return dest

def get_latest_version_cache(version_token: VersionToken, api_kind: APIKind) -> Optional[Path]:
    """
    This function checks if any cached version has is a valid `"latest"` one
    by checking control sums (file size and stored etag) with the ones requested from Factorio Developer Portal.
    
    Invokes at most one HEAD request to Factorio sites, and only if any cache file exists.
    
    Returns:
        * `Path` to the cache file of the latest version, if found
        * `None` otherwise
    """
    
    content_length: Optional[int] = None
    etag_value: Optional[str] = None
    
    for version in map(Path, glob.glob(str(get_cache_filepath('*', api_kind)))):
        if (content_length is None or etag_value is None):
            head = requests.head(get_download_url(version_token, api_kind))
            content_length = int(head.headers.get('Content-Length', '0'))
            etag_value = head.headers.get('Etag', '').strip('"')
            del head
        
        if (os.stat(str(version)).st_size == content_length and (etag:= etag_file(version)).exists() and etag.read_text() == etag_value):
            logger.debug(f"File '{version}' is a valid latest version")
            return version
        else:
            logger.debug(f"File '{version}' is a not a latest version")
    
    else:
        return None

def load_api(app_version: str = __LATEST__, api_kind: APIKind = APIKind.RuntimeAPI, *, api_version: int = DEFAULT_API_VERSION) -> FactorioDataModel:
    """
    Loads the `FactorioDataModel` from disk or, if missing, from Factorio Developer Portal.
    This function implements the source selecting logic.
    
    Args:
        app_version: `str`.
            Game version, semver string or the `"latest"` or `"stable"` keywords (see `__LATEST__`).
            Default value: `"latest"`.
        api_kind: `APIKind`.
            Which API to download. Either `APIKind.RuntimeAPI` or `APIKind.PrototypesAPI`.
        api_version: 
            The version of API.
            Changed only on a major releases, currently supported version is 3.
            Raises an `AssertionError` if API version is not equal.
            Default value: 5.
    
    Returns:
        `FactorioDataModel`
    """
    
    cache_file = get_cache_filepath(app_version, api_kind)
    if (cache_file.exists()):
        logger.debug(f"Cache '{cache_file}' exists")
    elif (app_version in _VERSION_TOKENS and (c := get_latest_version_cache(VersionToken(app_version), api_kind))):
        cache_file = c
    else:
        cache_file = download_api(cache_file, app_version=app_version, api_version=api_version, api_kind=api_kind)
    
    with cache_file.open('rb') as f:
        logger.info(f"Loading '{cache_file}'...")
        factorio_api: FactorioDataModel = json.load(f)
    
    assert factorio_api['api_version'] == api_version, f"Factorio API for app version {factorio_api['application_version']} has API version {factorio_api['api_version']} but {api_version} was expected."
    return factorio_api


__all__ = \
[
    'DEFAULT_API_VERSION',
    'APIKind',
    'VersionToken',
    '__LATEST__',
    '__STABLE__',
    
    'download_api',
    'get_cache_filepath',
    'get_download_url',
    'get_latest_version_cache',
    'load_api',
]
