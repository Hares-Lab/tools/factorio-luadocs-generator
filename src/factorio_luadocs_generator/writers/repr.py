import re
import warnings
from typing import *
from typing import Union

from ..model import *

LUA_RESERVED_KEYWORDS: Final[FrozenSet[str]] = frozenset \
((
    'and',
    'break',
    'do',
    'else',
    'elseif',
    'end',
    'false',
    'for',
    'function',
    'if',
    'in',
    'local',
    'nil',
    'not',
    'or',
    'repeat',
    'return',
    'then',
    'true',
    'until',
    'while',
))
def _name_repr(name: str) -> Iterator[str]:
    for k in name.split('.'):
        while (k in LUA_RESERVED_KEYWORDS):
            k += '_'
        yield k

@overload
def description_repr(item: BasicMember, inline: bool) -> str:
    pass

@overload
def description_repr(item: str, inline: bool) -> str:
    pass

@overload
def description_repr(item: Iterable[str], inline: bool) -> str:
    pass

def description_repr(item: Union[BasicMember, str, Iterable[str]], inline: bool) -> str:
    if (isinstance(item, str)):
        return description_repr(item.splitlines(), inline=inline) 
    elif (isinstance(item, dict) and 'description' in item):
        return description_repr(item['description'], inline=inline)
    elif (not inline):
        return '\n'.join(f"--- {line}" for line in item)
    else:
        return ' '.join(item)


def name_repr(name: str) -> str:
    if (':' in name):
        return ':'.join(map(name_repr, name.split(':')))
    return '.'.join(_name_repr(name))

_TOKEN_NAME_PATTERN = re.compile(r'[a-zA-Z]\w*')
def child_name_repr(name: str) -> str:
    return f'.{name}' if (_TOKEN_NAME_PATTERN.fullmatch(name)) else f'[{name!r}]'


def table_repr(params: Iterable[Parameter], variant_params: Optional[Iterable[ParameterGroup]] = None) -> str:
    params = list(params)
    if (variant_params):
        for pg in variant_params:
            params.extend(pg['parameters'])
    
    return '{{ {} }}'.format(', '.join(f"{name_repr(p['name'])}: {type_repr(p['type'])}" for p in params))

def cast_table_type(tp: AnyType) -> Optional[TableType]:
    if (isinstance(tp, dict)):
        tp: GenericType
        type_name = tp['complex_type']
        if (type_name == 'builtin'):
            return None
        elif (type_name == 'table'):
            return cast(TableType, tp)
        elif (type_name == 'tuple' and 'parameters' in tp):
            return cast(TableType, tp)
        elif (type_name in ('struct', 'LuaStruct')):
            tp: StructType
            return TableType(complex_type='table', parameters=tp.get('attributes', [ ]))
        elif \
        (
            type_name in ('LuaCustomTable', 'dictionary')
            and isinstance(cast(LuaCustomTable, tp)['key'], dict)
            and cast(GenericType, cast(LuaCustomTable, tp)['key'])['complex_type'] == 'union'
            and all((isinstance(option, dict) and cast(GenericType, option)['complex_type'] == 'literal') for option in cast(LuaCustomTable, tp)['key']['options'])
        ):
            # Extremely special case for something like that:
            # --- @alias SelectionModeFlags table<str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str|str,bool>
            tp: LuaCustomTable
            key: AnyType = tp['key']
            value = tp['value']
            return TableType(complex_type='table', parameters=[ Parameter(name=option['value'], type=value, order=i, description=option.get('description'), optional=False) for i, option in enumerate(cast(List[LiteralType], key['options'])) ])
    
    return None


def type_repr(tp: AnyType) -> str:
    if (isinstance(tp, str)):
        return tp
    
    elif (isinstance(tp, dict)):
        tp: GenericType
        type_name = tp['complex_type']
        
        if (None is not None):
            raise NotImplementedError
        
        elif (type_name == 'builtin'):
            # On this stage, the original name is lost.
            raise NotImplementedError
        
        elif (type_name == 'tuple' and (values := cast(TupleType, tp).get('values'))):
            return '{' + ','.join(map(type_repr, values)) + '}'
        
        elif (tab := cast_table_type(tp)):
            return table_repr(tab['parameters'], tab.get('variant_parameter_groups'))
        
        elif (type_name == 'union'):
            return '|'.join({ x: None for x in map(type_repr, cast(UnionType, tp)['options']) }.keys())
        
        elif (type_name == 'array'):
            return type_repr(cast(ArrayType, tp)['value']) + '[]'
        
        elif (type_name in ('LuaCustomTable', 'dictionary')):
            dic: DictionaryType = cast(DictionaryType, tp)
            return 'table<{},{}>'.format(type_repr(dic['key']), type_repr(dic['value']))
        
        elif (type_name == 'literal'):
            v = type(cast(LiteralType, tp)['value']).__name__
            if (v == 'str'):
                # Special case, all other types are already registered as names
                v = 'string'
            return v
        
        elif (type_name == 'function'):
            args = ', '.join(f"arg_{i+1}: {type_repr(param)}" for i, param in enumerate(cast(FunctionType, tp)['parameters']))
            return f"fun({args}): any"
        
        elif (type_name == 'type'):
            return type_repr(cast(TypeType, tp)['value'])
        
        else:
            warnings.warn(f"Potentially unsupported generic type detected: {type_name}.\n{tp}", category=UserWarning)
            return str(tp)
    
    else:
        raise TypeError(f"Unsupported type '{tp}'")

def value_repr(x: Any) -> str:
    match(x):
        case None: return 'nil'
        case bool(b): return str(b).lower()
        case _: return repr(x)


__all__ = \
[
    'LUA_RESERVED_KEYWORDS',
    
    'cast_table_type',
    'name_repr',
    'child_name_repr',
    'table_repr',
    'type_repr',
    'description_repr',
    'value_repr',
]
