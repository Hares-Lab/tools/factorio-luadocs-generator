import hashlib
import json
from io import StringIO, BytesIO
from os import PathLike
from typing import *
from pathlib import Path
import contextlib
from ..logging_utils import get_logger

logger = get_logger('writer')

__META_LINE__ = '--- @meta\n'
MAX_LINES_PER_FILE: Final[int] = 1000

SHA256_HASH_FILENAME = '.hashsum.sha256.json'

Checksum: TypeAlias = str

def read_hash(for_file: Path) -> Optional[Checksum]:
    hash_file = for_file.parent / SHA256_HASH_FILENAME
    if (hash_file.exists() and hash_file.is_file()):
        hashes: Dict[str, Checksum] = json.loads(hash_file.read_bytes())
        return hashes.get(for_file.name, None)
    else:
        return None

def write_hash(for_file: Path, checksum: Checksum):
    hash_file = for_file.parent / SHA256_HASH_FILENAME
    if (hash_file.exists() and hash_file.is_file()):
        hashes: Dict[str, Checksum] = json.loads(hash_file.read_bytes())
    else:
        hashes = dict()
    
    hashes[for_file.name] = checksum
    with hash_file.open('wt', encoding='utf-8') as f:
        json.dump(hashes, f, indent=4, sort_keys=True)

def get_hash_for(content: Union[str, bytes, StringIO, BytesIO], *, encoding='utf-8') -> Checksum:
    if (isinstance(content, StringIO)):
        content = content.read()
    if (isinstance(content, str)):
        content = content.encode(encoding=encoding)
    if (isinstance(content, bytes)):
        content = BytesIO(content)
    
    return hashlib.file_digest(content, 'sha256').hexdigest()

@contextlib.contextmanager
def write_file(filename: Union[str, PathLike], encoding='utf-8', skip_unchanged: bool = False):
    filename: Path = Path(filename)
    
    logger.info(f"Getting '{filename}' content...")
    str_io = StringIO()
    with contextlib.redirect_stdout(str_io):
        yield
    
    str_io.seek(0)
    text = str_io.read()
    lines = text.splitlines(keepends=True)
    
    current_hash = get_hash_for(text, encoding=encoding)
    if (not skip_unchanged and read_hash(filename) == current_hash):
        logger.info(f"File '{filename}' does not require an update.")
        return
    
    part_gen: Callable[[Any], Path] = lambda part_name: filename.with_stem(f'{filename.stem}-{part_name}')
    
    for f in filename.parent.glob(part_gen('*').name):
        logger.debug(f"Removing old part '{f}'...")
        f.unlink()
    
    with filename.open('wt', encoding=encoding) as f:
        if (len(lines) >= MAX_LINES_PER_FILE):
            parts: List[List[str]] = [ [ ] ]
            for block in text.split('\n\n'):
                current = parts[-1]
                block_lines = block.splitlines(keepends=True)
                block_lines.extend([ '\n', '\n' ])
                if (len(current) + len(block_lines) < MAX_LINES_PER_FILE):
                    current.extend(block_lines)
                else:
                    parts.append(block_lines)
            
            lines = [ __META_LINE__ ]
            for file_num, file_lines in enumerate(parts, start=1):
                part = part_gen(file_num)
                with part.open('wt', encoding=encoding) as part_io:
                    logger.debug(f"Writing '{filename}' part #{file_num}...")
                    part_io.writelines(lines)
                    part_io.writelines('\n')
                    part_io.writelines(file_lines)
                
                lines.append(f"require({'.' + part.with_suffix('').name!r})\n")
        
        logger.info(f"Writing '{filename}'...")
        f.writelines(lines)
    
    write_hash(filename, current_hash)
    logger.info(f"File '{filename}' successfully written on disk.")


__all__ = \
[
    '__META_LINE__',
    'MAX_LINES_PER_FILE',
    'write_file',
    'get_hash_for',
    'read_hash',
    'write_hash',
]
