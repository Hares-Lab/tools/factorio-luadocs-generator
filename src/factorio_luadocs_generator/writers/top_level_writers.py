import itertools
import math
from copy import copy, deepcopy
from typing import *

from .low_level_writers import write_class_impl, write_description_impl
from .repr import type_repr, name_repr, cast_table_type, child_name_repr
from ..model import *
from ..logging_utils import get_logger

logger = get_logger('top-level-writer')

# region Generics
def write_item(item: BasicMember, tp: Optional[str], value: str = '{ }', *, prefix: str = '', short: bool = False):
    if (item['description'] or not short):
        write_description_impl(item)
        print(f"--- ")
    if (tp or not short):
        print(f"--- @type {tp}")
        if (not short):
            print(f"----")
    print(f"{name_repr(prefix + item['name'])} = {value}")
    if (not short):
        print()

def write_concept(item: Union[RuntimeConcept, PrototypesConcept]):
    item_as_runtime = cast(RuntimeConcept, item)
    item_as_prototype = cast(PrototypesConcept, item)
    
    if ((tt:= cast_table_type(item_as_runtime['type'])) is not None):
        tt: TableType
        attributes = deepcopy(tt['parameters'])
        if (props := item_as_prototype.get('properties')):
            attributes.extend(itertools.chain.from_iterable(map(property_to_attribute, props)))
        return write_class_impl(item, attributes, methods=None, defaults=None, base=item_as_prototype.get('parent'))
    
    try:
        if ((ct := item_as_runtime['type']['complex_type']) == 'builtin'):
            logger.debug(f"Skipping builtin concept {item_as_runtime['name']!r}")
            return
    except (TypeError, KeyError):
        pass
    
    print(f"----")
    print(f"--- @alias {name_repr(item_as_runtime['name'])} {type_repr(item_as_runtime['type'])}")
    write_description_impl(item_as_runtime)
    print(f"----")
    print()
# endregion
# region Runtime API
def write_class(item: Class):
    parent = item.get('parent', None)
    if (parent is None):
        # Fallback to API < v5
        base_classes = item.get('base_classes', [])
        assert len(base_classes) < 2, "Cannot support classes with >= 2 bases"
        if (base_classes):
            parent = base_classes[0]
    
    write_class_impl(item, item['attributes'], methods=item['methods'], base=parent, is_global=True)

def write_define(item: Define, prefix: str = 'defines.'):
    child_prefix = name_repr(f'{prefix}{item["name"]}')
    
    values = sorted(item.get('values', [ ]), key=lambda x: x['order'])
    attrs = copy(values)
    subkeys = item.get('subkeys', [ ])
    for subkey in subkeys:
        attrs.append(Attribute(name=subkey['name'], description=subkey['description'], order=subkey['order'], type=child_prefix + child_name_repr(subkey['name']), read=True, write=False, optional=False))
    
    defaults: Dict[str, Any] = dict()
    for attr in values:
        defaults[attr['name']] = attr['order']
    
    write_class_impl(item, cast(List[Attribute], attrs), methods=None, defaults=defaults, default_attr_type=child_prefix, prefix=prefix, is_global=True, base='int')
    
    for subkey in subkeys:
        write_define(subkey, prefix=child_prefix + '.')
    
    print()

def write_global_object(item: GlobalObject):
    write_item(cast(BasicMember, item), type_repr(item['type']), '{ }')
# endregion
# region Prototypes API
def property_to_attribute(prop: Property) -> Iterator[Attribute]:
    prop: Property = deepcopy(prop)
    prop_as_attr: Attribute = cast(Attribute, prop)
    if ((default := prop.get('default')) and isinstance(default, str)): 
        prop_as_attr['description'] += '\n' f"Default value: {default}"
    prop_as_attr['read'] = True
    prop_as_attr['write'] = True
    
    yield prop_as_attr
    
    if (alt_name := prop.get('alt_name')):
        alt_attr: Attribute = copy(prop_as_attr)
        alt_attr['name'] = alt_name
        alt_attr['description'] = f"Alias to `{prop['name']}`"
        yield prop_as_attr

def custom_properties_to_attribute(prop: Optional[CustomProperties]) -> Iterator[Attribute]:
    if (not prop):
        return
    
    prop: CustomProperties = deepcopy(prop)
    prop_as_attr: Attribute = cast(Attribute, prop)
    
    prop_as_attr['order'] = math.inf
    prop_as_attr['name'] = '[{}]'.format(type_repr(prop['key_type']))
    prop_as_attr['type'] = prop['value_type']
    prop_as_attr['read'] = True
    prop_as_attr['write'] = True
    prop_as_attr['optional'] = True
    
    yield prop_as_attr

def write_prototype(item: Prototype):
    parent = item.get('parent', None)
    
    defaults: Dict[str, Any] = dict()
    for prop in item['properties']:
        if ((default := prop.get('default')) and isinstance(default, dict)):
            value = cast(LiteralType, default)['value']
            defaults[prop['name']] = value
    
    write_class_impl(item, itertools.chain(itertools.chain.from_iterable(map(property_to_attribute, item['properties'])), custom_properties_to_attribute(item.get('custom_properties', None))), methods=None, defaults=defaults, base=parent, is_global=True)
# endregion

__all__ = \
[
    'write_global_object',
    'write_concept',
    'write_class',
    'write_define',
    'write_item',
    'write_prototype',
]
