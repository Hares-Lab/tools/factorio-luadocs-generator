from typing import *

from ..model import *
from .repr import *


def write_description_impl(item: BasicMember):
    text = description_repr(item, inline=False)
    if (text):
        print(text)

def write_class_impl(item: BasicMember, attributes: Optional[Iterable[Attribute]], *, methods: Optional[Iterable[Method]] = None, defaults: Optional[Dict[str, Any]] = None, base: Optional[str] = None, prefix: str = '', default_attr_type = 'any', is_global: bool = False):
    name = name_repr(prefix + item['name'])
    
    print(f"----")
    print(f"--- @class {name}{(': ' + base) if (base) else ''}")
    if (cast(Prototype, item).get('deprecated')):
        print(f"--- @deprecated")
    write_description_impl(item)
    print(f"--- ")
    if (attributes):
        write_attributes_impl(attributes, 'field', default_attr_type=default_attr_type)
    print(f"----")
    if (is_global or defaults):
        print(f"{name} = {{ }}")
    if (defaults):
        for k, v in defaults.items():
            print(f"{name}{child_name_repr(k)} = {value_repr(v)}")
    print()
    if (methods):
        for meth in methods:
            write_method_impl(meth, prefix=f"{name}.")
        else:
            print()

def write_attributes_impl(attrs: Iterable[Union[Parameter, Attribute]], key='param', *, default_attr_type: str = 'any'):
    for attr in sorted(attrs, key=lambda a: a['order']):
        # Name is missing for return values
        print(f"--- @{key} {(name_repr(attr.get('name', '')) + ' ').lstrip()}{type_repr(attr.get('type', default_attr_type))}{ '?' if (attr.get('optional')) else '' } {description_repr(attr, inline=True)}")

def write_method_impl(method: Method, *, prefix: str = ''):
    takes_table: bool
    table_is_optional: Optional[bool]
    
    if ((method_format := method.get('format')) is not None):
        # API >= v5
        takes_table: bool = method_format['takes_table']
        table_is_optional = method_format.get('table_optional')
    else:
        # Fallback to API < v5
        takes_table = method['takes_table']
        table_is_optional = method.get('table_is_optional')
    
    name = name_repr(prefix + method['name'])
    method_params_name = f'{name}_parameters'.replace(':', '.')
    params = sorted(method['parameters'], key=lambda param: param['order'])
    
    if (takes_table):
        print(f"----")
        print(f"--- @class {method_params_name}")
        print(f"--- Parameters class for the method `{name}`")
        print(f"--- ")
        write_attributes_impl(params, key='field')
        print(f"----")
        print()
    
    print(f"----")
    # print(f"--- @function {name}")
    write_description_impl(method)
    print(f"--- ")
    
    args_list: str
    if (not takes_table):
        write_attributes_impl(params)
        args_list = ', '.join(name_repr(param['name']) for param in params)
    else:
        print(f"--- @param args {method_params_name}{ '?' if (table_is_optional) else '' } Table containing arguments for the method.")
        # print(f"--- @usage {name} {{ arg1='val1', arg2='val' }}")
        print(f"--- @see {method_params_name}")
        args_list = 'args'
    
    rv = method.get('return_values')
    if (rv):
        write_attributes_impl(rv, 'return')
    else:
        # print(f"--- @return nil Does not return any value")
        pass
    print(f"----")
    print(f"function {name}({args_list}) end")
    print()


__all__ = \
[
    'write_attributes_impl',
    'write_class_impl',
    'write_description_impl',
    'write_method_impl',
]
