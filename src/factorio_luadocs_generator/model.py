from __future__ import annotations

from typing import *

boolean = bool
string = str
number = Union[float, int]
array = List
union = Union
Operator = Union['Method', 'Attribute']
AnyType = Union['GenericType', str]

class FactorioDataModel(TypedDict):
    application: string
    """ The application this documentation is for. Will always be "factorio". """
    
    stage: string
    """ Indicates the stage this documentation is for. Will always be "runtime" (as opposed to "data"; see the data lifecycle for more detail). """
    
    application_version: string
    """ The version of the game that this documentation is for. An example would be "1.1.30". """
    
    api_version: number
    """ The version of the machine-readable format itself. It is incremented every time the format changes. The version this documentation reflects is stated at the top. """

class FactorioRuntimeDataModel(FactorioDataModel):
    stage: string = 'runtime'
    
    classes: array[Class]
    """ The list of classes (LuaObjects) the API provides. Equivalent to the classes page. """
    
    events: array[Event]
    """ The list of events that the API provides. Equivalent to the events page. """
    
    defines: array[Define]
    """ The list of defines that the game uses. Equivalent to the defines page. """
    
    builtin_types: NotRequired[array[BuiltinType]]
    """
    Deprecated in APIv5 in favor of `"builtin"` `RuntimeConcept`.    
    The list of types that are built into Lua itself. Equivalent to the built-in types page.
    """
    
    concepts: array[RuntimeConcept]
    """ The list of concepts of various types that the API uses. Equivalent to the concepts page. """
    
    global_objects: array[GlobalObject]
    """ The list of objects that the game provides as global variables to serve as entry points to the API. """
    
    global_functions: array[Method]
    """ The list of functions that the game provides as global variables to provide some specific functionality. """

class FactorioPrototypeDataModel(FactorioDataModel):
    stage: string = 'prototype'
    
    prototypes: array[Prototype]
    """ The list of prototypes that can be created. Equivalent to the prototypes page. """
    
    types: array[PrototypesConcept]
    """ The list of types (concepts) that the format uses. Equivalent to the types page. """


# region Top level types
class BasicMember(TypedDict):
    name: string
    """ The name of the member. """
    
    order: number
    """ The order of the member as shown in the html. """
    
    description: string
    """ The text description of the member. """
    
    lists: NotRequired[array[string]]
    """ A list of Markdown lists to provide additional information. Usually contained in a spoiler tag. """
    
    notes: NotRequired[array[string]]
    """
    Deprecated in API v5
    A list of strings containing additional information about the method.
    """
    
    examples: NotRequired[array[string]]
    """ A list of strings containing example code and explanations. """
    
    images: NotRequired[array[Image]]
    """ A list of illustrative images shown next to the member. """

class Prototype(BasicMember):
    visibility: NotRequired[array[string]]
    """ The list of game expansions needed to use this prototype. If not present, no restrictions apply. Possible values: "space_age". """
    
    parent: NotRequired[string]
    """ The name of the prototype's parent, if any. """
    
    abstract: boolean
    """ Whether the prototype is abstract, and thus can't be created directly. """
    
    typename: NotRequired[string]
    """ The type name of the prototype, like "boiler". null for abstract prototypes. """
    
    instance_limit: NotRequired[number]
    """ The maximum number of instances of this prototype that can be created, if any. """
    
    deprecated: boolean
    """ Whether the prototype is deprecated and shouldn't be used anymore. """
    
    properties: array[Property]
    """ The list of properties that the prototype has. May be an empty array. """
    
    custom_properties: NotRequired[CustomProperties]
    """ A special set of properties that the user can add an arbitrary number of. Specifies the type of the key and value of the custom property. """

class Class(BasicMember):
    visibility: NotRequired[array[string]]
    """ The list of game expansions needed to use this class. If not present, no restrictions apply. Possible values: "space_age". """
    
    methods: array[Method]
    """ The methods that are part of the class. """
    
    attributes: array[Attribute]
    """ The attributes that are part of the class. """
    
    operators: array[Operator]
    """ A list of operators on the class. They are called call, index, or length and have the format of either a Method or an Attribute. """
    
    abstract: boolean
    """ Whether the class is never itself instantiated, only inherited from. """
    
    parent: NotRequired[string]
    """ The name of the class that this class inherits from. """
    
    base_classes: NotRequired[array[string]]
    """
    Deprecated in APIv5 in favor of `parent` field.
    A list of the names of the classes that his class inherits from.
    """

class Event(BasicMember):
    data: array[Parameter]
    """ The event-specific information that is provided. """
    
    filter: NotRequired[string]
    """ The name of the filter concept that applies to this event. """

class Define(BasicMember):
    """
    Defines can be recursive in nature, meaning one Define can have multiple sub-Defines that have the same structure. These are singled out as subkeys instead of values.
    """
    
    values: NotRequired[array[BasicMember]]
    """ The members of the define. """
    
    subkeys: NotRequired[array[Define]]
    """ A list of sub-defines. """

class BuiltinType(TypedDict):
    name: string
    """ The name of the builtin type. """
    
    order: number
    """ The order of the builtin type as shown in the html. """
    
    description: string
    """ The text description of the builtin type. """

class RuntimeConcept(BasicMember):
    type: AnyType
    """
    The type of the concept.
    Either a proper Type, or the string `"builtin"`, indicating a fundamental type like string or number.
    """

class GlobalObject(TypedDict):
    name: string
    """ The global variable name of the object. """
    
    order: number
    """ The order of the global object as shown in the html. """
    
    description: string
    """ The text description of the global object. """
    
    type: string
    """ The class name of the global object. """
# endregion

# region Common structures
# Several data structures are used in different parts of the format, which is why they are documented separately to avoid repetition.
class Property(BasicMember):
    visibility: NotRequired[array[string]]
    """ The list of game expansions needed to use this property. If not present, no restrictions apply. Possible values: "space_age". """
    
    alt_name: NotRequired[string]
    """ An alternative name for the property. Either this or name can be used to refer to the property. """
    
    override: boolean
    """ Whether the property overrides a property of the same name in one of its parents. """
    
    type: TypeType
    """ The type of the property. """
    
    optional: boolean
    """ Whether the property is optional and can be omitted. If so, it falls back to a default value. """
    
    default: NotRequired[union[string, LiteralType]]
    """ The default value of the property. Either a textual description or a literal value. """

class CustomProperties(TypedDict):
    description: string
    """ The text description of the property. """
    
    lists: NotRequired[array[string]]
    """ A list of Markdown lists to provide additional information. Usually contained in a spoiler tag. """
    
    examples: NotRequired[array[string]]
    """ A list of code-only examples about the property. """
    
    images: NotRequired[array[Image]]
    """ A list of illustrative images shown next to the property. """
    
    key_type: TypeType
    """ The type of the key of the custom property. """
    
    value_type: TypeType
    """ The type of the value of the custom property. """

class PrototypesConcept(BasicMember):
    parent: NotRequired[string]
    """ The name of the type's parent, if any. """
    
    abstract: boolean
    """ Whether the type is abstract, and thus can't be created directly. """
    
    inline: boolean
    """ Whether the type is inlined inside another property's description. """
    
    type: AnyType
    """ The type of the type/concept (Yes, this naming is confusing). Either a proper Type, or the string "builtin", indicating a fundamental type like string or number. """
    
    properties: NotRequired[array[Property]]
    """ The list of properties that the type has, if its type includes a struct. null otherwise. """

class EventRaised(TypedDict):
    name: string
    """ The name of the event being raised. """
    
    order: number
    """ The order of the member as shown in the html. """
    
    description: string
    """ The text description of the raised event. """
    
    timeframe: string
    """ The timeframe during which the event is raised. One of "instantly", "current_tick", or "future_tick". """
    
    optional: boolean
    """ Whether the event is always raised, or only dependant on a certain condition. """

class GenericType(TypedDict):
    """
    A type is either a string, in which case that string is the simple type. Otherwise, a type is a table:
    Depending on `complex_type`, there are additional members.
    """
    
    complex_type: Required[string]
    """ A string denoting the kind of complex type. """

class TypeType(GenericType):
    complex_type: str = 'type'

    value: AnyType
    """ The actual type. This format for types is used when they have descriptions attached to them. """

    description: string
    """ The text description of the type. """

class UnionType(GenericType):
    complex_type: str = 'union'
    
    options: array[AnyType]
    """ A list of all compatible types for this type. """
    
    full_format: boolean
    """ Whether the options of this union have a description or not. """

class ArrayType(GenericType):
    complex_type: str = 'array'
    
    value: AnyType
    """ The type of the elements of the array. """

class DictionaryType(GenericType):
    complex_type: str = 'dictionary'
    
    key: AnyType
    """ The type of the keys of the dictionary or LuaCustomTable. """
    
    value: AnyType
    """ The type of the values of the dictionary or LuaCustomTable. """

class LuaCustomTable(DictionaryType):
    complex_type: str = 'LuaCustomTable'

class FunctionType(GenericType):
    complex_type: str = 'function'
    
    parameters: array[AnyType]
    """ The types of the function arguments. """

class LiteralType(GenericType):
    complex_type: str = 'literal'
    
    value: union[string, number, boolean]
    """ The value of the literal. """
    
    description: NotRequired[string]
    """ The text description of the literal, if any. """

class LuaLazyLoadedValue(GenericType):
    complex_type: str = 'LuaLazyLoadedValue'
    
    value: AnyType
    """ The type of the LuaLazyLoadedValue. """

class StructType(GenericType):
    complex_type: str = 'LuaStruct'
    
    attributes: array[Attribute]
    """ A list of attributes with the same properties as class attributes. """

class TableType(GenericType):
    complex_type: str = 'table'
    
    parameters: array[Parameter]
    """ The parameters present in the table. """
    
    variant_parameter_groups: NotRequired[array[ParameterGroup]]
    """ The optional parameters that depend on one of the main parameters. """
    
    variant_parameter_description: NotRequired[string]
    """ The text description of the optional parameter groups. """

class TupleType(TableType):
    complex_type = 'tuple'
    
    values: array[TypeType]
    """ The types of the members of this tuple in order. """


class Parameter(TypedDict):
    name: string
    """ The name of the parameter. """
    
    order: number
    """ The order of the parameter as shown in the html. """
    
    description: string
    """ The text description of the parameter. """
    
    type: AnyType
    """ The type of the parameter. """
    
    optional: boolean
    """ Whether the type is optional or not. """

class ParameterGroup(TypedDict):
    name: string
    """ The name of the parameter group. """
    
    order: number
    """ The order of the parameter group as shown in the html. """
    
    description: string
    """ The text description of the parameter group. """
    
    parameters: array[Parameter]
    """ The parameters that the group adds. """

class MethodFormat(TypedDict):
    takes_table: boolean
    """ Whether the method takes a single table with named parameters or a sequence of unnamed parameters. """
    
    table_optional: NotRequired[boolean]
    """ If `takes_table` is true, whether that whole table is optional or not. """

class Method(BasicMember):
    visibility: NotRequired[array[string]]
    """ The list of game expansions needed to use this method. If not present, no restrictions apply. Possible values: "space_age". """
    
    raises: NotRequired[array[EventRaised]]
    """ A list of events that this method might raise when called. """
    
    subclasses: NotRequired[array[string]]
    """ A list of strings specifying the sub-type (of the class) that the method applies to. """
    
    parameters: array[Parameter]
    """ The parameters of the method. How to interpret them depends on the takes_table member. """
    
    variant_parameter_groups: NotRequired[array[ParameterGroup]]
    """ The optional parameters that depend on one of the main parameters. Only applies if takes_table is true. """
    
    variant_parameter_description: NotRequired[string]
    """ The text description of the optional parameter groups. """
    
    variadic_parameter: NotRequired[VariadicParameter]
    """ The variadic parameter of the method, if it accepts any. """
    
    variadic_type: NotRequired[AnyType]
    """
    Deprecated in APIv5 in favor of `VariadicParameter`.
    The type of the variadic arguments of the method, if it accepts any.
    """
    
    variadic_description: NotRequired[string]
    """
    Deprecated in APIv5 in favor of `VariadicParameter`.
    The description of the variadic arguments of the method, if it accepts any.
    """
    
    format: MethodFormat
    """ Details on how the method's arguments are defined. """
    
    takes_table: NotRequired[boolean]
    """
    Deprecated in APIv5 in favor of `MethodFormat`.
    Whether the method takes a single table with named parameters or a sequence of unnamed parameters.
    """
    
    table_is_optional: NotRequired[boolean]
    """
    Deprecated in APIv5 in favor of `MethodFormat`.
    If `takes_table` is true, whether that whole table is optional or not.
    """
    
    return_values: array[Parameter]
    """ The return values of this method, which can contain zero, one, or multiple values. Note that these have the same structure as parameters, but do not specify a name. """

class VariadicParameter(TypedDict):
    type: NotRequired[Type]
    """ The type of the variadic arguments of the method, if it accepts any. """
    
    description: NotRequired[string]
    """ The description of the variadic arguments of the method, if it accepts any. """

class Attribute(BasicMember):
    visibility: NotRequired[array[string]]
    """ The list of game expansions needed to use this attribute. If not present, no restrictions apply. Possible values: "space_age". """
    
    raises: NotRequired[array[EventRaised]]
    """ A list of events that this attribute might raise when written to. """
    
    subclasses: NotRequired[array[string]]
    """ A list of strings specifying the sub-type (of the class) that the attribute applies to. """
    
    type: AnyType
    """ The type of the attribute. """
    
    optional: boolean
    """ Whether the attribute is optional or not. """
    
    read: boolean
    """ Whether the attribute can be read from. """
    
    write: boolean
    """ Whether the attribute can be written to. """

class Image(TypedDict):
    filename: string
    """ The name of the image file to display. These files are placed into the /static/images/ directory. """
    
    caption: NotRequired[string]
    """ The explanatory text to show attached to the image. """

# endregion

__all__ = \
[
    'FactorioDataModel',
    'FactorioRuntimeDataModel',
    'FactorioPrototypeDataModel',
    'BasicMember',
    'Class',
    'Event',
    'Define',
    'BuiltinType',
    'RuntimeConcept',
    'GlobalObject',
    'Prototype',
    'EventRaised',
    'AnyType',
    'GenericType',
    'UnionType',
    'ArrayType',
    'DictionaryType',
    'LuaCustomTable',
    'FunctionType',
    'LiteralType',
    'LuaLazyLoadedValue',
    'StructType',
    'TableType',
    'TupleType',
    'TypeType',
    'Parameter',
    'ParameterGroup',
    'VariadicParameter',
    'Method',
    'MethodFormat',
    'Attribute',
    'Property',
    'CustomProperties',
    'PrototypesConcept',
]
