import importlib.resources
import shutil
from pathlib import Path
from typing import *
from .io import get_local_build_directory
from .logging_utils import get_logger
from .model import *
from .writers.file_writer import write_file
from .writers.top_level_writers import *

build_directory = Path('.build')
logger = get_logger()

def write_static_resources(factorio_api: FactorioDataModel, *, dest_dir: Optional[Path] = None) -> Path:
    current_build_directory = get_local_build_directory(factorio_api, ensure_exists=True, parent=dest_dir)
    
    with importlib.resources.path(__package__, 'resources') as resources_directory:
        for file in resources_directory.rglob('*.lua'):
            rel_path = current_build_directory / file.relative_to(resources_directory)
            logger.info(f"Copying '{rel_path}'...")
            shutil.copy(file, rel_path)
    
    return current_build_directory

def write_runtime_api(factorio_api: FactorioRuntimeDataModel, dest_dir: Path = None) -> Path:
    current_build_directory = get_local_build_directory(factorio_api, parent=dest_dir, subtree=factorio_api['stage'], ensure_exists=True)
    
    with write_file(current_build_directory / 'classes.lua'):
        for cls in factorio_api['classes']:
            write_class(cls)
    
    with write_file(current_build_directory / 'concepts.lua'):
        for concept in factorio_api['concepts']:
            write_concept(concept)
    
    with write_file(current_build_directory / 'global_objects.lua'):
        for obj in factorio_api['global_objects']:
            write_global_object(obj)
    
    with write_file(current_build_directory / 'defines.lua'):
        print("--- @module")
        print("defines = { }")
        print()
        
        for obj in factorio_api['defines']:
            write_define(obj)
    
    return current_build_directory

def write_prototypes_api(factorio_api: FactorioPrototypeDataModel, runtime_api: FactorioRuntimeDataModel, dest_dir: Path = None) -> Path:
    current_build_directory = get_local_build_directory(factorio_api, parent=dest_dir, subtree=factorio_api['stage'], ensure_exists=True)
    
    with write_file(current_build_directory / 'prototypes.lua'):
        for cls in factorio_api['prototypes']:
            write_prototype(cls)
    
    runtime_concepts: Dict[str, RuntimeConcept] = { concept['name']: concept for concept in runtime_api['concepts'] }
    with write_file(current_build_directory / 'types.lua'):
        for concept in factorio_api['types']:
            if (concept['name'] in runtime_concepts): continue
            write_concept(concept)
    
    return current_build_directory


__all__ = \
[
    'write_runtime_api',
    'write_prototypes_api',
    'write_static_resources',
]
