from os import PathLike
from pathlib import Path
from typing import *

from .model import FactorioDataModel, FactorioRuntimeDataModel

BUILD_DIRECTORY: Final[Path]
CACHE_DIRECTORY: Final[Path]
AnyPath: TypeAlias = Union[PathLike, str]

@overload
def get_local_build_directory(factorio_api: FactorioDataModel, *, parent: Optional[Path] = None, subtree: Optional[AnyPath] = None, ensure_exists: bool = False, ensure_empty: bool = False) -> Path:
    pass
@overload
def get_local_build_directory(app_version: str, *, parent: Optional[Path] = None, subtree: Optional[AnyPath] = None, ensure_exists: bool = False, ensure_empty: bool = False) -> Path:
    pass

@overload
def get_local_cache_directory(factorio_api: FactorioDataModel, *, parent: Optional[Path] = None, subtree: Optional[AnyPath] = None, ensure_exists: bool = False, ensure_empty: bool = False) -> Path:
    pass
@overload
def get_local_cache_directory(app_version: str, *, parent: Optional[Path] = None, subtree: Optional[AnyPath] = None, ensure_exists: bool = False, ensure_empty: bool = False) -> Path:
    pass


__all__ = \
[
    'BUILD_DIRECTORY',
    
    'get_local_build_directory',
    'get_local_cache_directory',
]
