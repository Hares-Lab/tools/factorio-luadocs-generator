from functools import partial
from os import PathLike
from pathlib import Path
from typing import *

from .model import FactorioDataModel

BUILD_DIRECTORY: Final[Path] = Path('.build')
CACHE_DIRECTORY: Final[Path] = Path('.cache')

AnyPath: TypeAlias = Union[PathLike, str]

def _get_dir_impl \
(
    base: Path,
    subdir: Union[FactorioDataModel, str],
    *,
    parent: Optional[Path] = None,
    subtree: Optional[AnyPath] = None,
    ensure_exists: bool = False,
    ensure_empty: bool = False,
) -> Path:
    if (isinstance(subdir, str)):
        app_version = subdir
    elif (isinstance(subdir, dict)):
        app_version = cast(FactorioDataModel, subdir['application_version'])
    else:
        raise TypeError(f"Unsupported parameter value for parameter 'item': '{type(subdir)}' ({subdir!r})")
    
    result = (parent or base) / app_version
    if (subtree):
        result /= subtree
    
    if (ensure_empty and result.exists()):
        if (result.is_dir()):
            result.rmdir()
        else:
            result.unlink()
    
    if (ensure_exists):
        result.mkdir(exist_ok=True, parents=True, mode=0o755)
    
    return result

get_local_build_directory = partial(_get_dir_impl, BUILD_DIRECTORY)
get_local_cache_directory = partial(_get_dir_impl, CACHE_DIRECTORY)


__all__ = \
[
    'BUILD_DIRECTORY',
    
    'get_local_build_directory',
    'get_local_cache_directory',
]
