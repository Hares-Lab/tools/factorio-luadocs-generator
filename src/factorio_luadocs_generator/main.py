from typing import *
import sys
from pathlib import Path
from argparse import ArgumentParser

from factorio_luadocs_generator.model import FactorioRuntimeDataModel, FactorioPrototypeDataModel
from .api_loader import load_api, __LATEST__, APIKind
from .api_writer import write_runtime_api, write_prototypes_api, write_static_resources
from .logging_utils import get_logger, basic_config
from .io import BUILD_DIRECTORY

build_directory = Path('.build')
logger = get_logger('main')

class Arguments(NamedTuple):
    factorio_version: Optional[str] = None
    dest_dir: Optional[Path] = None
    verbose: bool = False
    

def make_api(params: Arguments) -> Path:
    logger.info("Starting Factorio Lua documentation generation...")
    
    runtime_api = cast(FactorioRuntimeDataModel, load_api(params.factorio_version, api_kind=APIKind.RuntimeAPI))
    target = write_runtime_api(runtime_api, dest_dir=params.dest_dir)
    logger.info(f"Factorio Runtime Lua documentation successfully written at '{target}'.")
    
    if (runtime_api['api_version'] >= 4):
        prototypes_api = cast(FactorioPrototypeDataModel, load_api(runtime_api['application_version'], api_kind=APIKind.PrototypesAPI))
        target = write_prototypes_api(prototypes_api, runtime_api, dest_dir=params.dest_dir)
        logger.info(f"Factorio Prototypes Lua documentation successfully written at '{target}'.")
    
    target = write_static_resources(runtime_api, dest_dir=params.dest_dir)
    logger.info(f"Factorio Prototypes Lua documentation successfully written at '{target}'.")
    
    return target


def create_argument_parser() -> ArgumentParser:
    from . import __version__, __title__
    
    name = "Factorio LuaDocs Generator"
    version_string = f"{__title__} v{__version__}"
    
    parser = ArgumentParser(prog=__title__, add_help=False, description=f"{name}\n\n{version_string}\nDownloads Factorio runtime API specification in JSON format and writes it to the LuaDocs (https://stevedonovan.github.io/ldoc/) format, so it can be used in IDEs.")
    parser.add_argument('--factorio-version', '-F', default=__LATEST__, help="The Factorio game version for which API would be generated.")
    parser.add_argument('--dest-dir', '-d', type=Path, default=BUILD_DIRECTORY, help="The root directory where the Lua API documentation will be stored.")
    
    options = parser.add_argument_group(title="Program Options")
    options.add_argument('--version', '-V', action='version', version=version_string, help="Show version and exit")
    options.add_argument('--help', '-h', action='help', help="Show this message and exit")
    options.add_argument('--verbose', '-v', action='store_true', help="Increases the verbosity of the program.")
    
    return parser


def main(*args):
    if (not args and len(sys.argv) > 1):
        args = sys.argv[1:]
    
    parser = create_argument_parser()
    params: Arguments = cast(Arguments, parser.parse_args(args))
    
    basic_config(level='DEBUG' if (params.verbose) else 'INFO')
    make_api(params)
    
    return 0


__all__ = \
[
    'Arguments',
    
    'create_argument_parser',
    'main',
    'make_api',
]


if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
