from logging import getLogger as _get_logger, basicConfig as basic_config, Logger

__BASE_NAME__ = __package__.replace('_', '-')

from typing import *


@overload
def get_logger(logger: Logger, name: str) -> Logger:
    pass
@overload
def get_logger(name: str) -> Logger:
    pass
@overload
def get_logger() -> Logger:
    pass

def _get_logger_base() -> Logger:
    return _get_logger(__BASE_NAME__)
def _get_logger_by_name(name: str) -> Logger:
    return _get_logger(f'{__BASE_NAME__}.{name}')
def _get_logger_by_logger(logger: Logger, name: str) -> Logger:
    return _get_logger(f'{logger.name}.{name}')

_logger_fns: Dict[int, Callable[[Any], Logger]] = \
{
    0: _get_logger_base,
    1: _get_logger_by_name,
    2: _get_logger_by_logger,
}

def get_logger(*args, **kwargs) -> Logger:
    return _logger_fns[len(args) + len(kwargs)](*args, **kwargs)


__all__ = \
[
    '__BASE_NAME__',
    
    'get_logger',
    'basic_config',
]
