# noinspection PyProtectedMember
from setuptools import _install_setup_requires
_install_setup_requires(dict(setup_requires=[ 'extended-setup-tools' ]))

from extended_setup import ExtendedSetupManager
ExtendedSetupManager('factorio_luadocs_generator').setup \
(
    name = 'factorio-luadocs-generator',
    category = 'tools',
    license = "BSD 2-Clause License",
    short_description = "Downloads Factorio runtime API specification in JSON format and writes it to the LuaDocs (https://stevedonovan.github.io/ldoc/) format, so it can be used in IDEs.",
    entry_points = { 'console_scripts': 'factorio-luadocs-generator = factorio_luadocs_generator.main:main' },
    min_python_version = '3.11.0',
    classifiers =
    [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Topic :: Games/Entertainment :: Real Time Strategy',
        'Topic :: Games/Entertainment :: Simulation',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
    ]
)
