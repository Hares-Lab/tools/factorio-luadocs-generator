# Factorio Luadocs Generator
![logo-medium](docs/img/logo-medium.png)

Downloads [Factorio Runtime API] & [Factorio Prototype Definitions] specification in JSON format and writes it to the [LuaDoc] format, so it can be used in IDEs.

### Examples
#### VSCode + [EmmyLua]
![VSCode + EmmyLua](docs/img/vscode.png)

### Usage
#### Just use the Precompiled LuaDocs
If you are not interested in generating docs for the specific version of Factorio,
or automating linters to check your mod uses the correct API,
you can just download the precompiled package either from [this link](https://disk.yandex.ru/d/hl4RPLKKP-ph9w)
or ~~from the releases section of this repository~~ *(Not yet implemented)*.

You should then extract the archive content
and mark the directory as an additional sources root or library root for your project.

#### ~~Install from `pip`~~
*Not yet implemented.*

#### Install from Source Code
To install from sources, you will need Python 3.9 or higher.
In the project directory, call the following
(virtual environment usage is encouraged):
```
$ python -m pip install -e .
```

This will download and install all dependencies,
as well as create `factorio-luadocs-generator` executable
(an alias for `python -m factorio_luadocs_generator`).
You can use it to generate the LuaDocs with the required settings, such as:
 * Any Factorio version: `-F FACTORIO_VERSION,`
 * Any destination directory: `-d DEST_DIR`

Once you've done generating the LuaDocs,
you can import them to your IDE as described above.

To see all available options, check `factorio-luadocs-generator --help`.

### Project Status
 - :white_check_mark: Downloads and caches `runtime-api.json` & `prototype-api.json`
 - :white_check_mark: Parses `runtime-api.json` & `prototype-api.json`
 - :white_check_mark: Supports API from v3 (1.1.62) to v5 (1.1.108) inclusively
 - Writes the following sources of data:
   * :white_check_mark: Classes
   * :white_check_mark: Concepts
   * :white_check_mark: Defines
   * :white_check_mark: Globals
   * :white_check_mark: Prototypes
 - Writes the following information for each type or object
   * :white_check_mark: Parents
   * :white_check_mark: Description
   * :white_check_mark: Fields or Attributes (with type)
   * :white_check_mark: Methods
   * :x: Examples, notes, lists fields
 - :x: Does not handle the case where lua table should have predefined string keys containing dashes (`-`) (Partially solved in 0.1.1)
 - :x: Does not have support for variadic arguments
 - :x: Does not handle the "one of" classes with the discriminator field, those are counted as having all possible attributes
 - :x: EmmyLua plugin for JetBrains IDE's could *sometimes* not display methods (weird!)
 - :x: Automate release creation via GitLab CI for different Factorio versions

### Useful Links
 * [Lua Official Website][Lua]
 * [LuaDoc]
 * [Factorio Official Site][Factorio]
 * [Factorio Prototype Definitions]
 * [Factorio Runtime API]


<!--- Links --->
[Factorio Prototype Definitions]: https://lua-api.factorio.com/stable/index-prototype.html
[Factorio Runtime API]: https://lua-api.factorio.com/stable/index-runtime.html
[Factorio]: https://factorio.com/
[Lua]: https://www.lua.org/
[LuaDoc]: https://stevedonovan.github.io/ldoc/
[EmmyLua]: https://github.com/EmmyLua
